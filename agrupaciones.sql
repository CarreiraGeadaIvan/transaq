USE tempdb 
GO
-- si no queremos usar tempdb tambien podemos 
-- crear una nueva base de datos para este ejemplo

--DROP DATABASE IF EXISTS multitabla
--GO
--USE multitabla
--GO


DROP TABLE IF EXISTS Productos
GO
DROP TABLE IF EXISTS Frutas
GO

-- Sentencia equivalente al IF EXISTS anterior
-- pero usada en versiones antiguas

--IF object_id('Productos') IS NOT NULL
--	DROP TABLE Productos
--GO
--IF object_id('Frutas') IS NOT NULL
--	DROP TABLE Productos
--GO

CREATE TABLE Productos (
	Nombre NVARCHAR(20),
	Precio SMALLMONEY)
GO

CREATE TABLE Frutas(
	Nombre NVARCHAR(20),
	Precio SMALLMONEY)
GO

INSERT Productos
	VALUES ('Leche', 2.4),
		('Miel', 4.99),
		('Manzanas', 3.99),
		('Pan', 2.45),
		('Uvas', 4.00)
GO

INSERT Frutas
	VALUES ('Manzanas', 6.0),
		('Uvas', 4.00),
		('Bananas', 4.95),
		('Mandarinas', 3.95),
		('Naranjas', 2.50)
GO

SELECT * FROM Productos 
	ORDER BY Nombre
GO

--Nombre	Precio
--Leche		2,40
--Manzanas	3,99
--Miel		4,99
--Pan		2,45
--Uvas		4,00

SELECT * FROM Frutas
	ORDER BY Nombre
GO

--Nombre		Precio
--Bananas		4,95
--Mandarinas	3,95
--Manzanas		6,00
--Naranjas		2,50
--Uvas			4,00

---- UNION (sin filas duplicadas)

SELECT Nombre FROM Productos 
UNION
SELECT Nombre FROM Frutas
GO
--No muestra los campos duplicados
--Nombre
--Bananas
--Leche
--Mandarinas
--Manzanas
--Miel
--Naranjas
--Pan
--Uvas

-- Si queremos que muestre los campos duplicados
-- utilizaremos la clausula ALL (UNION ALL)

SELECT Nombre FROM Productos
UNION ALL
SELECT Nombre FROM Frutas
ORDER BY Nombre DESC
GO

--Nombre
--Uvas
--Uvas
--Pan
--Naranjas
--Miel
--Manzanas
--Manzanas
--Mandarinas
--Leche
--Bananas

SELECT Nombre, Precio FROM Productos
UNION 
SELECT Nombre, Precio FROM Frutas 
GO
-- Tampoco muestra campos duplicados
-- la fruta uvas tiene el mismo precio
-- en las dos tablas, as� que solo muestra 
-- un registro
--Nombre		Precio
--Bananas		4,95
--Leche			2,40
--Mandarinas	3,95
--Manzanas		3,99
--Manzanas		6,00
--Miel			4,99
--Naranjas		2,50
--Pan			2,45
--Uvas			4,00


--Una vez m�s, si queremos que nos muestre los 
-- campos duplicados usamos la cl�usula ALL

SELECT Nombre, Precio FROM Productos
UNION ALL
SELECT Nombre, Precio FROM Frutas 
ORDER BY Nombre, Precio
GO

--Nombre		Precio
--Bananas		4,95
--Leche			2,40
--Mandarinas	3,95
--Manzanas		3,99
--Manzanas		6,00
--Miel			4,99
--Naranjas		2,50
--Pan			2,45
--Uvas			4,00
--Uvas			4,00