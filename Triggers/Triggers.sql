
---------------------------------------
---------------------------------------
--------------TRIGGERS-----------------
---------------------------------------
---------------------------------------


---------------------------------------
------TRIGGER A NIVEL DE SERVIDOR------
---------------------------------------
--Este tipo de trigger se almacena en--
-----------'Server Objects'------------
---------------------------------------

USE master
GO

--Deshabilitar nuevos Logins en una instancia SQL
DROP TRIGGER IF EXISTS trg_NoNuevoLogin
GO

CREATE TRIGGER trg_NoNuevoLogin
ON ALL SERVER
FOR CREATE_LOGIN --Sentencia a controlar, puede haber m�s de una
AS
	PRINT 'No login creations without DBA involvement'
	ROLLBACK TRAN
GO

--Intentamos crear un login y nos va a dar el error
CREATE LOGIN Joe WITH PASSWORD='1234D'
GO

--No login creations without DBA involvement
--Msg 3609, Level 16, State 2, Line 32
--The transaction ended in the trigger. The batch has been aborted.

DROP TRIGGER trg_NoNuevoLogin
GO

--Msg 3701, Level 11, State 5, Line 39
--Cannot drop the trigger 'trg_NoNuevoLogin', because it does not exist or you do not have permission.
--Lo hacemos a trav�s del GUI, en serverObjects

------------------------------------------
------------------------------------------
--TRIGGER A NIVEL DE BASE DE DATOS (DML)--
------------------------------------------
--En el entorno gr�fico aparecen dentro --
-----de 'Programmability' de cada BD------

USE pubs
GO

--Primero creamos una tabla con SELECT INTO

DROP TABLE IF EXISTS Autores
GO

SELECT * INTO Autores 
FROM Authors
GO

--Creamos el Trigger 

DROP TRIGGER IF EXISTS trg_PrevenirBorrado
GO

CREATE TRIGGER trg_PrevenirBorrado
ON DATABASE
FOR DROP_TABLE, ALTER_TABLE
AS
	RAISERROR('No se puede borrar o modificar tablas', 16,3)
	ROLLBACK TRAN
GO

--Probamos a borrar la tabla reci�n creada
DROP TABLE Autores
GO

--Msg 50000, Level 16, State 3, Procedure trg_PrevenirBorrado, Line 5 [Batch Start Line 78]
--No se puede borrar o modificar tablas
--Msg 3609, Level 16, State 2, Line 79
--The transaction ended in the trigger. The batch has been aborted.

DISABLE TRIGGER trg_PrevenirBorrado ON DATABASE
GO

ENABLE TRIGGER trg_PrevenirBorrado ON DATABASE
GO

--Para borrar
DROP TRIGGER trg_PrevenirBorrado
GO
--Msg 3701, Level 11, State 5, Line 94
--Cannot drop the trigger 'trg_PrevenirBorrado', because it does not exist or you do not have permission.

--Si da Error de Borrado hacerlo desde el GUI


---------------------------------------------
---------------------------------------------
------TRIGGER A NIVEL DE TABLA O VISTA-------
---------------------------------------------
--Se almacenan en la taba o vista a la que --
---------------van asociados-----------------
---------------------------------------------


USE Pubs 
GO

DROP TABLE IF EXISTS Autores
GO

SELECT * INTO Autores
FROM Authors
GO

DROP TRIGGER IF EXISTS trg_DarAutor
GO

--Creamos un Trigger que nos ejecute un raiserror y un procedimiento almacenado
-- Despu�s de una insercion o un update en la tabla de autores

CREATE TRIGGER trg_DarAutor
ON Autores
FOR INSERT, UPDATE
--O FOR AFTER
AS
	RAISERROR (50009,16,10)
	EXEC sp_helpdb pubs
GO

--Comprobamos el contenido de la tabla
SELECT * FROM Autores
GO

--Lo probamos
UPDATE Autores 
	SET au_lname='Black'
	WHERE au_fname='Johnson'
GO

--Msg 18054, Level 16, State 1, Procedure trg_DarAutor, Line 6 [Batch Start Line 140]
--Error 50009, severity 16, state 10 was raised, but no message with that error number was found in sys.messages. 
--If error is larger than 50000, make sure the user-defined message is added using sp_addmessage.
 

--(1 row affected)

-----Devuelve el codigo del error pero la sentencia se ejecuta

--au_id			au_lname	au_fname	phone			address			city		state	zip		contract
--172-32-1176	Black		Johnson		408 496-7223	10932 Bigge Rd.	Menlo Park	CA		94025	1

--Deshabilitamos y habilitamos

DISABLE TRIGGER trg_DarAutor ON autores
GO

ENABLE TRIGGER trg_DarAutor ON autores
GO

DROP TRIGGER trg_DarAutor
GO


---Creamos otro Trigger de tipo AFTER

USE pubs
GO

IF EXISTS (SELECT NAME FROM sysobjects
			WHERE name='trg_borra' AND type='tr')
			DROP TRIGGER trg_borra
GO


CREATE TRIGGER trg_borra
ON Autores
FOR DELETE, UPDATE
AS 
	RAISERROR ('%d filas modificadas en la tabla Autores', 16, 1, @@rowcount)
GO

---Mensajes de error con variables globales, '%d' es el valor (int) de la variable
--- @@rowcount <-- variable global, cuenta las filas afectadas en una operaci�n
--- @@trancount <-- cuenta las filas afectadas en una transacci�n
--- @@error <-- devuelve el codigo del error

--Try out
DELETE Autores 
WHERE au_fname='Johnson'
GO
 
--Sigue siendo un trigger after, por lo cual lanza el mensaje despu�s de borrar
--Msg 50000, Level 16, State 1, Procedure trg_borra, Line 6 [Batch Start Line 193]
--1 filas modificadas en la tabla Autores

--(1 row affected)


---- TRIGGER SOBRE UNA VISTA
-- Va a ser un trigger de tipo INSTEAD OF, ejecutar� el contenido del trigger
-- en lugar de la sentencia que lo dispara

-- Creamos la vista
CREATE VIEW vAutores
AS 
	SELECT * FROM Autores
GO

--Creamos un trigger para la vista (INSTEAD OF)
CREATE TRIGGER trg_BorrarVista
ON vAutores
INSTEAD OF DELETE
AS
	PRINT 'No se pueden borrar registros'
GO

SELECT * FROM vAutores
GO

DELETE vAutores
GO

--No se pueden borrar registros
--(22 rows affected)

SELECT * FROM vAutores
GO

-------------------------
---TRIGGER CON TABLAS TEMPORALES INSERTED DELETED
---------------------------------------------------

USE pubs
GO

DROP TRIGGER IF EXISTS trg_TablasTemporales
GO

CREATE TRIGGER trg_TablasTemporales
ON Autores
FOR UPDATE
AS
	SELECT au_id, au_lname, au_fname, city
		FROM INSERTED
	SELECT au_id, au_lname, au_fname, city	
		FROM DELETED
GO

SELECT * FROM Autores 
	WHERE au_fname='Marjorie'
GO
--Antes del update
--au_id			au_lname	au_fname	phone			address				city	state	zip		contract
--213-46-8915	Green		Marjorie	415 986-7020	309 63rd St. #411	Oakland	CA		94618	1


--Ejecutamos el update y automaticamente el trigger
UPDATE Autores
SET au_lname='VERDE'
WHERE au_fname='Marjorie'
GO

---TABLA INSERTED
--au_id			au_lname	au_fname
--213-46-8915	VERDE		Marjorie

--TABLA DELETED
--au_id			au_lname	au_fname
--213-46-8915	Green		Marjorie


--Otro ejemplo pero actualizando varios registros a la vez
--Usamos el mismo trigger anterior

UPDATE Autores
SET city='A Coru�a'
WHERE contract=0
GO

--INSERTED
--au_id			au_lname	au_fname	city
--893-72-1158	McBadden	Heather		A Coru�a
--724-08-9931	Stringer	Dirk		A Coru�a
--527-72-3246	Greene		Morningstar	A Coru�a
--341-22-1782	Smith		Meander		A Coru�a

--DELETED
--au_id			au_lname	au_fname	city
--893-72-1158	McBadden	Heather		Vacaville
--724-08-9931	Stringer	Dirk		Oakland
--527-72-3246	Greene		Morningstar	Nashville
--341-22-1782	Smith		Meander		Lawrence


-----------------------------------------------------------------
--TRIGGER CON ESTRUCTURA CONDICIONAL SOBRE UN CAMPO
-----------------------------------------------------------------

USE pubs
GO

DROP TRIGGER IF EXISTS trg_ActualizarCiudad
GO

CREATE TRIGGER trg_ActualizarCiudad
ON Autores
FOR UPDATE
AS 
	IF UPDATE(city)
		BEGIN
			RAISERROR ('No se puede cambiar la ciudad de residencia', 15, 1)
			ROLLBACK TRAN
		END
	ELSE
		PRINT 'Operaci�n Realizada'
GO

UPDATE Autores
SET au_lname='VERDE'
WHERE au_lname='green'
GO
--au_id	au_lname	au_fname	city
--213-46-8915	VERDE	Marjorie	Oakland

--Operaci�n Realizada

UPDATE Autores
SET city = 'Bilbado'
WHERE au_lname='VERDE'
GO


--Msg 50000, Level 15, State 1, Procedure trg_ActualizarCiudad, Line 7 [Batch Start Line 332]
--No se puede cambiar la ciudad de residencia
--Msg 3609, Level 16, State 1, Line 333
--The transaction ended in the trigger. The batch has been aborted.

SELECT city FROM Autores WHERE au_lname='VERDE'
GO
--city
--Oakland

---------------------------------
---------------------------------
-----Vamos con los de pensar-----
---------------------------------
---------------------------------

USE Northwind
GO

DROP TABLE IF EXISTS Empleados
GO

SELECT EmployeeID, LastName
INTO Empleados
FROM Employees
GO

SELECT * FROM Empleados ORDER BY EmployeeID
GO

--Crear un trigger que no permita borrar m�s de un registro con una sentencia delete

DROP TRIGGER IF EXISTS trg_UnSoloBorrado
GO

CREATE TRIGGER trg_UnSoloBorrado
ON Empleados
FOR DELETE
AS	
	IF (@@ROWCOUNT = 0)
		BEGIN 
			PRINT ('No se han encontrado registros')
			ROLLBACK
			RETURN
		END
	ELSE IF (@@ROWCOUNT>1)
	---OTRA FORMA DE HACER EL IF ES CONTAR LOS REGISTROS DE LA TABLA DELETED)
	-- IF (SELECT COUNT(*) FROM DELETED)>1
		BEGIN 
			RAISERROR ('No se pueden hacer borrados masivos, solo una fila cada vez', 16, 3)
			ROLLBACK TRAN
			RETURN --rompe la ejecucion y sale del trigger
		END
	ELSE
		PRINT ('Borrado correcto')
GO


DELETE Empleados
GO
--Msg 50000, Level 16, State 1, Procedure trg_UnSoloBorrado, Line 7 [Batch Start Line 384]
--No se pueden hacer borrados masivos, solo una fila cada vez
--Msg 3609, Level 16, State 1, Line 385
--The transaction ended in the trigger. The batch has been aborted.

SELECT * FROM Empleados ORDER BY EmployeeID
GO

--EmployeeID	LastName
--1	Davolio
--2	Fuller
--3	Leverling
--4	Peacock
--5	Buchanan
--6	Suyama
--7	King
--8	Callahan
--9	Dodsworth

DELETE Empleados
WHERE EmployeeID = 1
GO

--EmployeeID	LastName
--2	Fuller
--3	Leverling
--4	Peacock
--5	Buchanan
--6	Suyama
--7	King
--8	Callahan
--9	Dodsworth

DELETE Empleados
WHERE EmployeeID=99
GO

--No se han encontrado registros
--Msg 3609, Level 16, State 1, Line 432
--The transaction ended in the trigger. The batch has been aborted.



CREATE TRIGGER trg_RecursosHumanos
ON Empleados
INSTEAD OF DELETE 
AS 
	BEGIN 
		DECLARE @Count INT
		SET @Count = @@ROWCOUNT
		IF @Count = 0
			RETURN
		SET NOCOUNT ON
		BEGIN
			RAISERROR('No se pueden borrar empleados, solo marcarlos como inactivos', 10, 1)
			IF @@TRANCOUNT>0
				BEGIN 
					ROLLBACK TRAN
				END
		END
	END
GO

DELETE Empleados
GO
--No se pueden borrar empleados, solo marcarlos como inactivos
--Msg 3609, Level 16, State 1, Line 462
--The transaction ended in the trigger. The batch has been aborted.