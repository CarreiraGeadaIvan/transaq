-- Controlar las operaciones de Insertar y Borrar
--Crear una tabla auditor�a para la tabla ProductInventory de AdventureWorks2014 en la que se almacenar�n los registros insertados y borrados m�s una columna que nos diga el tipo de registro 'I' para insertados y 'D'para borrados
--Vamos a a�adir dos restricciones al trigger anterior:
--  a)Que no nos deje insertar elementos en la shelf '�' por Control Stocks
--  b)Que no nos deje borrar art�culos con cantidad mayor que cero

--
USE AdventureWorks2014;
GO
IF OBJECT_ID('Production.ProductInventoryAudit','U') is not null
DROP TABLE Production.ProductInventoryAudit
GO
CREATE TABLE Production.ProductInventoryAudit (
		ProductID int not null,
		LocationID smallint not null,
		Shelf nvarchar(10) not null,
		Bin tinyint not null,
		Quantity smallint not null,
		rowguid uniqueidentifier not null,
		ModifiedDate date DEFAULT GETDATE() ,
		InsertOrDelete char(1) not null
			check (InsertOrDelete in('I','D')));
GO
SELECT * 
FROM Production.ProductInventoryAudit
GO


IF OBJECT_ID('Production.Inventario','U') is not null
DROP TABLE Production.Inventario
GO
SELECT *
INTO Production.Inventario
FROM Production.ProductInventory
GO

--Start Trigger
--Controlamos su existencia

DROP TRIGGER IF EXISTS Production.trg_uid_ProductInventoryAudit
GO

SELECT * FROM Production.Inventario
GO

SELECT * FROM Production.ProductInventoryAudit
GO

CREATE TRIGGER Production.trg_uid_ProductInventoryAudit
ON Production.Inventario
AFTER INSERT, DELETE
AS
	IF EXISTS (
		SELECT Shelf FROM inserted
			WHERE Shelf = 'a' OR Shelf='A'
			)
			BEGIN 
				PRINT 'No se pueden insertar arituclos en la estanteria A'
				ROLLBACK TRAN 
				RETURN
			END
	INSERT Production.ProductInventoryAudit (ProductID, LocationID, Shelf, Bin, Quantity, rowguid, ModifiedDate, InsertOrDelete)
		SELECT DISTINCT i.ProductID, i.LocationID, i.Shelf, i.Bin, i.Quantity, i.rowguid, GETDATE(), 'I'
		FROM inserted i

-- Intenta borrar un articulo con existencias
	IF EXISTS (
		SELECT Quantity FROM deleted
			WHERE Quantity>0
			)
			BEGIN 
				PRINT 'No se pueden eliminar productos de los que hay existencias'
				ROLLBACK TRAN
				RETURN
			END
	INSERT Production.ProductInventoryAudit (ProductID, LocationID, Shelf, Bin, Quantity, rowguid, ModifiedDate, InsertOrDelete)
		SELECT DISTINCT d.ProductID, d.LocationID, d.Shelf, d.Bin, d.Quantity, d.rowguid, GETDATE(), 'D'
		FROM deleted d
GO


----- Provamos el trigger
Insert Production.Inventario (ProductID,LocationID,Shelf,Bin,Quantity,[rowguid],ModifiedDate)
	Values (450,2,'A',4,22,NEWID(),GETDATE());
GO

--No se pueden insertar arituclos en la estanteria A
--Msg 3609, Level 16, State 1, Line 82
--The transaction ended in the trigger. The batch has been aborted.

delete Production.Inventario
	where ProductID = 679 
GO

--No se pueden eliminar productos de los que hay existencias
--Msg 3609, Level 16, State 1, Line 90
--The transaction ended in the trigger. The batch has been aborted.

Insert Production.Inventario (ProductID,LocationID,Shelf,Bin,Quantity,[rowguid],ModifiedDate)
	Values (555,2,'C',4,22,NEWID(),GETDATE());
GO

UPDATE Production.Inventario
SET Quantity = 0
WHERE ProductID = 555 and LocationID = 2
GO

DELETE Production.Inventario
WHERE ProductID = 555 and LocationID = 2
GO

SELECT * FROM Production.ProductInventoryAudit
GO

--ProductID	LocationID	Shelf	Bin	Quantity	rowguid									ModifiedDate	InsertOrDelete
--555		2			C		4	22			90B44390-86DE-4189-B6DD-6F4D79097BB0	2018-05-31		I
--555		2			C		4	0			90B44390-86DE-4189-B6DD-6F4D79097BB0	2018-05-31		D