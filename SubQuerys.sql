--DROP DATABASE IF EXISTS multitabla
--GO
--USE multitabla
--GO

USE master
GO

CREATE DATABASE multitabla
GO

USE multitabla
GO

DROP TABLE IF EXISTS Productos
GO
DROP TABLE IF EXISTS Frutas
GO

-- Sentencia equivalente al IF EXISTS anterior
-- pero usada en versiones antiguas

--IF object_id('Productos') IS NOT NULL
--	DROP TABLE Productos
--GO
--IF object_id('Frutas') IS NOT NULL
--	DROP TABLE Productos
--GO

CREATE TABLE Productos (
	Nombre NVARCHAR(20),
	Precio SMALLMONEY)
GO

CREATE TABLE Frutas(
	Nombre NVARCHAR(20),
	Precio SMALLMONEY)
GO

INSERT Productos
	VALUES ('Leche', 2.4),
		('Miel', 4.99),
		('Manzanas', 3.99),
		('Pan', 2.45),
		('Uvas', 4.00)
GO

INSERT Frutas
	VALUES ('Manzanas', 6.0),
		('Uvas', 4.00),
		('Bananas', 4.95),
		('Mandarinas', 3.95),
		('Naranjas', 2.50)
GO


SELECT Nombre, Precio
FROM Productos
WHERE Nombre = (SELECT Nombre
	FROM Frutas)
GO
--Msg 512, Level 16, State 1, Line 57
--Subquery returned more than 1 value. This is not permitted when the subquery 
--follows =, !=, <, <= , >, >= or when the subquery is used as an expression.

SELECT Nombre, Precio
FROM Productos
WHERE Nombre IN (SELECT Nombre
	FROM Frutas)
GO

SELECT Nombre, Precio
FROM Productos
WHERE Precio < (SELECT MAX(Precio)
	FROM Frutas)
GO

SELECT Nombre, Precio
FROM Productos
WHERE Precio > (SELECT MIN(Precio)
	FROM Frutas)
GO

SELECT Nombre, Precio
FROM Productos
WHERE Precio < (SELECT AVG(Precio)
	FROM Frutas)
GO

USE Northwind
GO

SELECT CompanyName FROM Customers
WHERE CustomerID = (SELECT CustomerID 
	FROM Orders 
	WHERE OrderID = 10290)
GO

SELECT c.CompanyName 
FROM Customers c JOIN Orders o
ON c.CustomerID = o.CustomerID
WHERE OrderID = 10290
GO

-- Usando Pubs, Nombres de Editoriales que publican libros de Negocios

USE pubs
GO

SELECT p.pub_name
FROM publishers p
WHERE p.pub_id IN (
		SELECT t.pub_id
		FROM titles t
		WHERE t.type = 'business')
GO

--pub_name
--New Moon Books
--Algodata Infosystems

--
--Nombres de Editoriales que publican libros de Negocios
--

SELECT p.pub_name
FROM publishers p
WHERE p.pub_id NOT IN (
		SELECT t.pub_id
		FROM titles t
		WHERE t.type = 'business')
GO

--pub_name
--Binnet & Hardley
--Five Lakes Publishing
--Ramona Publishers
--GGG&G
--Scootney Books
--Lucerne Publishing

SELECT p.pub_name
FROM publishers p
WHERE p.pub_id IN (
		SELECT t.pub_id
		FROM titles t
		WHERE t.type != 'business')
GO

--pub_name
--New Moon Books
--Binnet & Hardley
--Algodata Infosystems

SELECT p.pub_name
FROM publishers p
WHERE EXISTS(
	SELECT * FROM titles t
	WHERE p.pub_id = t.pub_id
		AND t.type = 'business')
GO

--pub_name
--New Moon Books
--Algodata Infosystems


SELECT p.pub_name
FROM publishers p
WHERE NOT EXISTS(
	SELECT * FROM titles t
	WHERE p.pub_id = t.pub_id
		AND t.type = 'business')
GO

--pub_name
--Binnet & Hardley
--Five Lakes Publishing
--Ramona Publishers
--GGG&G
--Scootney Books
--Lucerne Publishing

SELECT p.pub_name
FROM publishers p
WHERE EXISTS(
	SELECT * FROM titles t
	WHERE p.pub_id = t.pub_id
		AND t.type != 'business')
GO

--pub_name
--New Moon Books
--Binnet & Hardley
--Algodata Infosystems

SELECT DISTINCT p.pub_name
FROM publishers p JOIN titles t
ON p.pub_id = t.pub_id
WHERE t.type = 'business'
GO

--pub_name
--Algodata Infosystems
--New Moon Books

--Find the names of authors who live in the city in which Algodata Infosystems is located

SELECT a.au_fname, a.au_lname
FROM authors a 
WHERE a.city = (
		SELECT p.city
		FROM publishers p 
		WHERE p.pub_name = 'Algodata Infosystems')
GO

--au_fname	au_lname
--Cheryl	Carson
--Abraham	Bennet