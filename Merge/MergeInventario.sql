USE Fusionar
GO 
DROP TABLE IF EXISTS BookInventory;
 CREATE TABLE BookInventory -- target 
 ( TitleID INT NOT NULL PRIMARY KEY, Title NVARCHAR(100) NOT NULL, 
 Quantity INT NOT NULL CONSTRAINT Quantity_Default_1 DEFAULT 0 ); 
DROP TABLE IF EXISTS BookOrder;
 CREATE TABLE BookOrder -- source 
 ( TitleID INT NOT NULL PRIMARY KEY, Title NVARCHAR(100) NOT NULL, Quantity INT NOT NULL 
 CONSTRAINT Quantity_Default_2 DEFAULT 0 );

INSERT BookInventory VALUES 
	(1, 'The Catcher in the Rye', 6), 
	(2, 'Pride and Prejudice', 3), 
	(3, 'The Great Gatsby', 0), 
	(5, 'Jane Eyre', 0), 
	(6, 'Catch 22', 0), 
	(8, 'Slaughterhouse Five', 4); 
INSERT BookOrder VALUES 
	(1, 'The Catcher in the Rye', 3), 
	(3, 'The Great Gatsby', 0), 
	(4, 'Gone with the Wind', 4), 
	(5, 'Jane Eyre', 5), 
	(7, 'Age of Innocence', 8);

MERGE BookInventory bi
USING BookOrder bo
ON bi.TitleID = bo.TitleID
WHEN MATCHED THEN
	UPDATE
	SET bi.Quantity = bi.Quantity + bo.Quantity;

SELECT * FROM BookInventory
GO

MERGE BookInventory bi
USING BookOrder bo
ON bi.TitleID = bo.TitleID
WHEN MATCHED AND
	bi.Quantity + bo.Quantity = 0 THEN
	DELETE;
		
SELECT * FROM BookInventory;

CREATE PROC MergeInventario
AS 
	BEGIN 
		DROP TABLE IF EXISTS NuevoInventario
		CREATE TABLE NuevoInventario(
			ActionType NVARCHAR(20),
			DelNombre NVARCHAR(60), DelCantidad varchar(12), 
			InsNombre NVARCHAR(60), InsCantidad varchar(12));
		MERGE INTO BookInventory bi
		USING BookOrder bo
		ON (bi.TitleID = bo.TitleID)
			WHEN MATCHED AND
				bi.Quantity + bo.Quantity = 0 THEN
				DELETE;
			--WHEN MATCHED THEN 
			--	UPDATE
			--	SET bi.Quantity = bi.Quantity + bo.Quantity
			--WHEN NOT MATCHED BY TARGET THEN
			--	INSERT (TitleID, Title, Quantity)
			--	VALUES (bo.TitleID, bo.Title, bo.Quantity)
			--WHEN NOT MATCHED BY SOURCE
			--AND bi.Quantity = 0 THEN
			--	DELETE;
			--OUTPUT $action, DELETED.Title, DELETED.Quantity,
			--	INSERTED.Title, INSERTED.Quantity
			--INTO NuevoInventario;
		SELECT * FROM NuevoInventario
	END
GO

EXEC MergeInventario
GO