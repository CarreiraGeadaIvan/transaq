CREATE PROC MergeEnTabla
AS
	BEGIN
		DROP TABLE IF EXISTS Combinada
		CREATE TABLE Combinada(
			ActionType NVARCHAR(10), DelNombre NVARCHAR(60), DelPrecio varchar(12),
			InsNombre NVARCHAR(60), InsPrecio varchar(12));
		MERGE INTO Productos -- Target
		USING Frutas --Source
		ON (Productos.Nombre = Frutas.Nombre)
			WHEN MATCHED THEN 
				UPDATE SET Precio=Frutas.Precio
			WHEN NOT MATCHED BY TARGET THEN
				INSERT (Nombre, Precio)
				VALUES (Frutas.Nombre, Frutas.Precio)
			WHEN NOT MATCHED BY SOURCE THEN 
				DELETE
			OUTPUT $action, DELETED.*, INSERTED.* INTO Combinada;
		SELECT * FROM Combinada ORDER BY ActionType
	END
GO

EXEC MergeEnTabla
GO

