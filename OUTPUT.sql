USE AdventureWorks2014
GO

DROP TABLE IF EXISTS VentasPersona
GO

SELECT Bonus, CommissionPct, SalesQuota	
	INTO VentasPersona
	FROM Sales.SalesPerson
	WHERE (Bonus = 0)
GO

SELECT * FROM VentasPersona
GO

-- Actualizar todos los campos de determinadas columnas de una tabla
-- NO EJECUTAR!!!!

UPDATE Person.Address
SET ModifiedDate = GETDATE()
GO
------EJECUTAR A PARTIR DE AQU�

UPDATE dbo.VentasPersona
	SET Bonus = 6000, CommissionPct = .10, SalesQuota = NULL
GO

SELECT * FROM VentasPersona
GO

DROP TABLE IF EXISTS Empleados
GO
SELECT * INTO Empleados FROM HumanResources.Employee
GO 


SELECT TOP(10) VacationHours FROM Empleados
GO

UPDATE TOP (10) Empleados
	SET VacationHours = VacationHours*1.25
GO

SELECT TOP(10) VacationHours FROM Empleados
GO

-- Actualiza el valor de la columna Color de la tabla Production.Product
-- para todas las filas con un valor existente de 'Red' en la columna Color
-- y con un valor que comience por 'Road-250' en la columna Name.


SELECT * INTO Productos FROM Production.Product
GO 

UPDATE Productos
	SET Color = 'Rojo Metalizado'
	WHERE Color = 'Red'
		AND Name LIKE 'Road-250%'
GO

SELECT * FROM Productos 
WHERE Color = 'Rojo Metalizado'
GO

--Actualizan las horas de vacaciones de los 10 empleados cuyas fechas de alta
-- son las mas antiguas

SELECT TOP(10) BusinessEntityID, HireDate, VacationHours FROM Empleados
ORDER BY HireDate ASC
GO

UPDATE Empleados
SET VacationHours = VacationHours*1.25
FROM (SELECT TOP 10 BusinessEntityID FROM Empleados
	ORDER BY HireDate ASC) AS th
WHERE Empleados.BusinessEntityID=th.BusinessEntityID
GO

UPDATE Empleados
SET VacationHours = VacationHours*1.25
WHERE HireDate IN (
	SELECT TOP(10) HireDate FROM Empleados
	ORDER BY HireDate ASC
	)
GO

-- Clausula Output - Variable Tabla - Update - Stored Procedure

USE AdventureWorks2014
GO

-- Incrementando dias de vacaciones para los 10 primeros empleados 
-- y mostrando los dias que tenian antes, los que tienen ahora y la 
-- fecha de modificaci�n

SELECT * FROM HumanResources.Employee
go

CREATE PROC VariarVacaciones
AS
	BEGIN
		DROP TABLE IF EXISTS  Empleados
		SELECT * INTO Empleados FROM HumanResources.Employee
		
		DECLARE @MyTableVar TABLE(
			EmpID int NOT NULL,
			OldVacationHours int,
			NewVacationHours int,
			ModifiedDate datetime
			)

		UPDATE TOP(10) Empleados 
			SET VacationHours = VacationHours * 2,
				ModifiedDate = GETDATE()
			OUTPUT inserted.BusinessEntityID,
					deleted.VacationHours,
					inserted.VacationHours,
					inserted.ModifiedDate
			INTO @MyTableVar
		
		SELECT TOP(10) BusinessEntityID, VacationHours, ModifiedDate FROM Empleados 
		SELECT * FROM @MyTableVar
	END
GO

EXEC VariarVacaciones
GO

CREATE PROC AumentarVacaciones_parametro @Variacion int
AS 
	BEGIN
		DROP TABLE IF EXISTS  Empleados
		SELECT * INTO Empleados FROM HumanResources.Employee
		
		DECLARE @MyTableVar TABLE(
			EmpID int NOT NULL,
			OldVacationHours int,
			NewVacationHours int,
			ModifiedDate datetime
			)

		UPDATE TOP(10) Empleados 
			SET VacationHours = VacationHours + @Variacion,
				ModifiedDate = GETDATE()
			OUTPUT inserted.BusinessEntityID,
					deleted.VacationHours,
					inserted.VacationHours,
					inserted.ModifiedDate
			INTO @MyTableVar
		
		SELECT TOP(10) BusinessEntityID, VacationHours, ModifiedDate FROM Empleados 
		SELECT * FROM @MyTableVar
	END
GO

EXEC AumentarVacaciones_parametro '10'
GO

------ o tambi�n

EXEC AumentarVacaciones_parametro @Variacion=15
GO
