-- SELECT 

-- Checking the Database Server Version 

-- AT == @ == VARIABLE LOCAL
-- @@ == VARIABLE GLOBAL, PROPIA DEL SISTEMA

SELECT @@VERSION
GO
--(No column name)
--Microsoft SQL Server 2016 (RTM) - 13.0.1601.5 (X64)   Apr 29 2016 23:23:58   Copyright (c) Microsoft Corporation  Enterprise Edition (64-bit) on Windows 10 Pro 6.3 <X64> (Build 10240: ) (Hypervisor) 

PRINT @@VERSION
GO
--Microsoft SQL Server 2016 (RTM) - 13.0.1601.5 (X64) 
--	Apr 29 2016 23:23:58 
--	Copyright (c) Microsoft Corporation
--	Enterprise Edition (64-bit) on Windows 10 Pro 6.3 <X64> (Build 10240: ) (Hypervisor)

SELECT DB_NAME();
GO 
--(No column name)
--master

USE AdventureWorks2014
GO
PRINT DB_NAME();
GO
--AdventureWorks2014

SELECT ORIGINAL_LOGIN(), CURRENT_USER, SYSTEM_USER;
--(No column name)	(No column name)	(No column name)
--ICG-W10\ICG			dbo					ICG-W10\ICG

PRINT ORIGINAL_LOGIN();
--ICG-W10\ICG

PRINT CURRENT_USER;
--dbo

PRINT SYSTEM_USER;
--ICG-W10\ICG

-- Listar todo el contenido de una tabla/vista
-- Se pueden poner todos los campos separados por coma

USE AdventureWorks2014
GO
SELECT * FROM HumanResources.Employee;
GO
--
-- Otra opcion si no estamos en la base de datos

USE master 
GO 
SELECT * FROM AdventureWorks2014.HumanResources.Employee
GO
--

--Listar solo unos campos determinados

USE AdventureWorks2014
GO

SELECT NationalIDNumber, LoginID
FROM HumanResources.Employee
GO

--Concatenaci�n : Unir cadenas

SELECT Title, FirstName + LastName as 'Nombre Persona'
FROM Person.Person
GO

--SELECT TITLE AS 'TITULO' Y APELLIDOS, NOMBRE

SELECT Title AS 'Titulo', LastName+', '+FirstName AS 'Apellidos Nombre'
FROM Person.Person
GO  

--Evitar duplicados
SELECT DISTINCT Title FROM Person.Person
GO

--Filtrados WHERE
SELECT Title, FirstName, LastName
FROM Person.Person
WHERE Title='Ms.' or Title='Ms'
GO

SELECT Title, FirstName, LastName
FROM Person.Person
WHERE Title<>'Ms.'
GO

SELECT Title, FirstName, LastName
FROM Person.Person
WHERE Title!='Ms.'
GO

SELECT Title, FirstName, LastName
FROM Person.Person
WHERE Title IS NULL
GO

SELECT Title, FirstName, LastName
FROM Person.Person
WHERE Title IS NOT NULL
GO

--Selecionar una se�ora y que su apellito sea Antrim
-- este cumple dos condiciones por lo que es una expresion
-- logica compuesta con AND

SELECT Title, FirstName, LastName
FROM Person.person
WHERE Title='Ms.'
	AND LastName='Antrim'
GO
--Title	FirstName	LastName
--Ms.	Ramona	Antrim

--Hacemos lo mismo con OR
SELECT Title, FirstName, LastName
FROM Person.person
WHERE Title='Ms.'
	OR LastName='Antrim'
GO
--(415 rows affected)

--NEGANDO
SELECT Title, FirstName, LastName
FROM Person.person
WHERE NOT Title='Ms.'
GO
--(594 rows affected)

--Aqui muestra tanto los que cumplen la condicion de que sea Ms. 
--como los que tienen de nombre Catherine o de apellido Adams.
--Prioridad Operadores: Uso de Par�ntesis 
-- Cuando en una instrucci�n se usa m�s de un operdor l�gico
--primero se eval�a NOT, luego AND y finalmente OR.

SELECT Title, FirstName, LastName
FROM Person.person
WHERE Title='Ms.'
	AND FirstName ='Catherine'
	OR LastName ='Adams'
GO
--(88 rows affected)

SELECT Title, FirstName, LastName
FROM Person.person
WHERE Title='Ms.'
	OR LastName ='Adams'
	AND FirstName ='Catherine'
GO
--(415 rows affected)

SELECT Title, FirstName, LastName
FROM Person.person
WHERE Title='Ms.'
	AND (FirstName ='Catherine'
	OR LastName ='Adams')
GO
--(4 rows affected)

--Prioridad Operadores: Uso de Par�ntesis 
-- Cuando en una instrucci�n se usa m�s de un operdor l�gico
--primero se eval�a NOT, luego AND y finalmente OR.


SELECT Title, FirstName, LastName
	FROM Person.Person
	WHERE (Title='Ms.' AND FirstName = 'Catherine')
		OR LastName = 'Adams'
GO
--(88 rows affected)

SELECT Title, FirstName, LastName
	FROM Person.Person
	WHERE Title='Ms.' AND (FirstName = 'Catherine'
		OR LastName = 'Adams')
GO
--(4 rows affected)


--CAMPOS CALCULADOS
SELECT BusinessEntityID, 
		VacationHours + SickLeaveHours AS AvaibleTimeOFF
	FROM HumanResources.Employee
GO

--BusinessEntityID	AvaibleTimeOFF
--1					168
--2					21
--3					23
--4					128
--5					27
--...					....
-- Sumamos dos columnas de una misma fila y las mostramos, pero no se almacenan


-- ALIAS: Providing shorthand name for schemas/tables
-- ALIAS E para HumanResources.Employee

SELECT E.BusinessEntityID AS "EmployeeID",
		E.VacationHours AS "Vacation",
		E.SickLeaveHours AS "Sick Time"
FROM HumanResources.Employee AS E; 

--EmployeeID	Vacation	Sick Time
--1				99			69
--2				1			20
--3				2			21

--LIMITAR RESULTADOS

SELECT JobTitle, HireDate
FROM HumanResources.Employee
GO
--(290 rows affected)

SELECT TOP 5 JobTitle, HireDate
FROM HumanResources.Employee
GO
--(5 rows affected)

SELECT TOP 5 PERCENT JobTitle, HireDate
FROM HumanResources.Employee
GO
--(15 rows affected)

--
--DISTINCT: Evita el duplicado de contenidos
--

SELECT JobTitle
FROM HumanResources.Employee
GO
--(290 rows affected)

SELECT DISTINCT JobTitle
FROM HumanResources.Employee
GO
--(67 rows affected)

--
--BETWEEN - IN 
--

SELECT SalesOrderID, ShipDate
FROM Sales.SalesOrderHeader
WHERE ShipDate BETWEEN '2014-07-04'
	AND '2014-07-05'
GO
--(63 rows affected)

-- Lo mismo usando operaciones l�gicas compuestas
SELECT SalesOrderID, ShipDate
FROM Sales.SalesOrderHeader
WHERE ShipDate >= '2014-07-04' AND
	  ShipDate <= '2014-07-05'
GO
--(63 rows affected)

--Utilizar n�meros en lugar de fechas
SELECT SalesOrderID, ShipDate
FROM Sales.SalesOrderHeader
WHERE SalesOrderID BETWEEN 43659 AND 43662
GO
--SalesOrderID	ShipDate
--43659	2011-06-07 00:00:00.000
--43660	2011-06-07 00:00:00.000
--43661	2011-06-07 00:00:00.000
--43662	2011-06-07 00:00:00.000

--Con una expresion l�gica compuesta
SELECT SalesOrderID, ShipDate
FROM Sales.SalesOrderHeader
WHERE (SalesOrderID >= 43659) AND (SalesOrderID <= 43662)
GO
--SalesOrderID	ShipDate
--43659	2011-06-07 00:00:00.000
--43660	2011-06-07 00:00:00.000
--43661	2011-06-07 00:00:00.000
--43662	2011-06-07 00:00:00.000


--IN: Pertenencia a conjuntos
-- Preguntamos por varios colores

SELECT ProductID, Name, Color
	FROM Production.Product
	WHERE Color IN ('Silver', 'BLACK', 'Red')
GO
--(174 rows affected)

--Sustituyendo IN por OR
SELECT ProductID, Name, Color
	FROM Production.Product
	WHERE Color = 'Silver' or Color = 'BLACK' or Color = 'Red'
GO
--(174 rows affected)

--Aqui usamos NOT IN para que aparezcan en la consulta todos menos estos
--tres colores
SELECT ProductID, Name, Color
	FROM Production.Product
	WHERE Color NOT IN ('Silver', 'BLACK', 'Red')
GO
--(82 rows affected)


--Paging through a resul set

USE Northwind
GO
SELECT LastName+' '+FirstName AS "Nombre de Empleado"
FROM Employees
ORDER BY LastName
GO
--(9 rows affected)

--OFFSET ELEMINA LAS x PRIMERAS LINEAS
SELECT LastName+' '+FirstName AS "Nombre de Empleado"
FROM Employees
ORDER BY LastName
OFFSET 3 ROWS
GO

--FETCH NEXT x ROWS ONLY -> muestra las x siguientes a partir del offset
SELECT LastName+' '+FirstName AS "Nombre de Empleado"
FROM Employees
ORDER BY LastName
OFFSET 1 ROWS FETCH NEXT 5 ROWS ONLY
GO
--(5 rows affected)

--WITH TIES ENLACES

USE tempdb
GO
CREATE TABLE FamousTransformers
(ID SMALLINT PRIMARY KEY IDENTITY(1,1),
FirstName VARCHAR(100) NOT NULL,
LastName VARCHAR(100) NOT NULL,
FavouriteColour VARCHAR(50) NOT NULL);
GO
--populate with some data (6 rows to demonstrate this working)
INSERT INTO FamousTransformers(FirstName, LastName, FavouriteColour)
VALUES('Clark','Kent','Blue');
INSERT INTO FamousTransformers(FirstName, LastName, FavouriteColour)
VALUES('Bruce','Wayne','Black');
INSERT INTO FamousTransformers(FirstName, LastName, FavouriteColour)
VALUES('Peter','Parker','Red');
INSERT INTO FamousTransformers(FirstName, LastName, FavouriteColour)
VALUES('Optimus','Prime','Red');
INSERT INTO FamousTransformers(FirstName, LastName, FavouriteColour)
VALUES('David','Banner','Green');
INSERT INTO FamousTransformers(FirstName, LastName, FavouriteColour)
VALUES('Rodimus','Prime','Orange');


-- A�adir
INSERT INTO FamousTransformers(FirstName, LastName, FavouriteColour)
VALUES('Manuel','Arias','Red');

--SELECT USING NORMAL TOP X

SELECT TOP 7 FirstName, LastName, FavouriteColour 
FROM FamousTransformers
ORDER BY FavouriteColour
GO

--WITH TIES
SELECT TOP 7 WITH TIES FirstName, LastName, FavouriteColour 
FROM FamousTransformers
ORDER BY FavouriteColour
GO

INSERT INTO FamousTransformers(FirstName, LastName, FavouriteColour)
VALUES('Pedro','Piqueras','Red');
INSERT INTO FamousTransformers(FirstName, LastName, FavouriteColour)
VALUES('Pepito','Perez','Orange');
INSERT INTO FamousTransformers(FirstName, LastName, FavouriteColour)
VALUES('Fulanito','DeTal','Orange');


-- En la primera posicion una B o una M
USE pubs
GO
SELECT au_lname, au_fname FROM authors
WHERE au_lname LIKE '[BM]%'
GO
--au_lname		au_fname
--Bennet			Abraham
--Blotchet-Halls	Reginald
--MacFeather		Stearns
--McBadden		Heather

SELECT au_lname, au_fname FROM authors
WHERE au_lname NOT LIKE '[BM]%'
GO
--(19 rows affected)

-- no empiece por B,M
SELECT au_lname, au_fname FROM authors
WHERE au_lname LIKE '[^BM]%'
GO
--(19 rows affected)

SELECT au_lname, au_fname FROM authors
WHERE au_lname LIKE '[Ck]ars[ei]n'
GO
--au_lname	au_fname
--Karsen		Livia

-- En la primera posicion a b c d (rango)
SELECT au_lname, au_fname FROM authors
WHERE au_fname LIKE '[A-D]%'
GO
--(10 rows affected)

USE AdventureWorks2014
GO
SELECT Name, ListPrice, ProductNumber FROM Production.Product
WHERE ProductNumber LIKE 'FR-_[0-9][0-9]_-[0-9][0-9]'
GO
--(79 rows affected)


--CASTS Y FECHAS
SELECT convert(varchar, getdate(),0)
GO
--(No column name)
--Apr 20 2018  6:31PM

SELECT convert(varchar, getdate(), 1)
GO
--(No column name)
--04/20/18

SELECT convert(varchar, getdate(), 2)
GO
--(No column name)
--18.04.20

SELECT convert(varchar, getdate(), 106)
GO
--(No column name)
--20 Apr 2018

SELECT convert(varchar, getdate(), 110)
GO
--(No column name)
--04-20-2018

SELECT convert(varchar, getdate(), 111)
GO
--(No column name)
--2018/04/20

--FORMATOS DE TIEMPO (HORAS)

SELECT convert(varchar, getdate(), 8)
GO
--18:37:27

SELECT convert(varchar, getdate(), 114)
GO
--18:37:48:167

--FECHAS Y TIEMPO 
SELECT convert(varchar, getdate(), 0)
GO
--Apr 20 2018  6:38PM

SELECT convert(varchar, getdate(), 13)	
GO
--20 Apr 2018 18:38:57:537


-- Concatenate SQL Server Columns into a String with CONCAT()
USE AdventureWorks2014
GO
SELECT 
    Title, 
    FirstName, 
    MiddleName, 
    LastName,
    Title+ ' ' + FirstName + ' ' + MiddleName + ' ' + LastName as MailingName
FROM Person.Person
GO
--(19972 rows affected)

SELECT 
    Title, 
    FirstName, 
    MiddleName, 
    LastName,
    ISNULL(Title,'') + ' ' + ISNULL(FirstName,'') + ' ' 
  + ISNULL(MiddleName,'') + ' ' + ISNULL(LastName,'') as MailingName
FROM Person.Person
GO
--(19972 rows affected)


SELECT 
    Title, 
    FirstName, 
    MiddleName, 
    LastName,
    CONCAT(Title,' ',FirstName,' ',MiddleName,' ',LastName) as MailingName
FROM Person.Person
GO
--(19972 rows affected)


--ORDER BY
--
-- Creamos

-----------------------------
USE tempdb
GO

DROP TABLE IF EXISTS Libros
GO

--IF OBJECT_ID ('Libros') IS NOT NULL
--	DROP TABLE Libros ;

CREATE TABLE Libros (
	codigo INT IDENTITY,
	titulo VARCHAR(40) NOT NULL,
	autor VARCHAR(20) DEFAULT 'Desconocido',
	editorial VARCHAR(20),
	precio DECIMAL(6,2),
	cantidad TINYINT,
	PRIMARY KEY(codigo) );

INSERT INTO libros
	VALUES ('El aleph', 'Borges', 'Planeta', 15, NULL);
INSERT INTO libros
	VALUES ('Martin Fierro', 'Jose Hernandez', 'Emece', 22.20, 200);
INSERT INTO libros
	VALUES ('Antologia poetica', 'J.L. Borges', 'Planeta', NULL, 150);
INSERT INTO libros
	VALUES ('Aprenda PHP', 'Mario Molina', 'Emece', 18.20, NULL);
INSERT INTO libros
	VALUES ('Cervantes y el quijote', 'Bioy Casares- J.L. Borges', 'Paidos', NULL, 100);
INSERT INTO libros
	VALUES ('Manual de PHP', 'J.C. Paez', 'Siglo XXI', 31.80, 120);
INSERT INTO libros
	VALUES ('Harry Potter y la piedra filosofal', 'J.K. Rowling', DEFAULT, 45.00, 90);
INSERT INTO libros
	VALUES ('Harry Potter y la camara secreta', 'J.K. Rowling', 'Emece', 46.00, 100);
INSERT INTO libros (titulo, autor, cantidad)
	VALUES ('Alicia en el pais de las maravillas', 'Lewis Carroll', 220);
INSERT INTO libros (titulo, autor, cantidad)
	VALUES ('PHP de la A a la Z', DEFAULT, 0);
go
select * from Libros
go

SELECT * FROM Libros 
	ORDER BY editorial
GO

--(9 rows affected)
SELECT * FROM Libros
	WHERE editorial IS NOT NULL
	ORDER BY editorial
GO
--(6 rows affected)

SELECT titulo, autor, editorial FROM Libros 
	ORDER BY editorial, titulo DESC
GO

SELECT * FROM Libros 
	WHERE editorial IS NOT NULL
	ORDER BY editorial, titulo DESC
GO


--Recuperar registros ordenados por un campo calculado

SELECT titulo, autor, editoriaL, precio-(precio*0.1) AS 'Precio con descuento'
FROM Libros
ORDER BY 4
GO

SELECT titulo, autor, editoriaL, precio-(precio*0.1) AS 'Precio con descuento'
FROM Libros
ORDER BY 'Precio con descuento'
GO

----- ORDENAR POR FECHAS
USE AdventureWorks2014
GO
SELECT BusinessEntityID, JobTitle, HireDate, DATEPART(year, HireDate) AS 'A�o Contrato'
FROM HumanResources.Employee
ORDER BY DATEPART(year, HireDate);
GO

--DATEPART -> toma la parte de la fecha (year) del campo concreto (HireDate) 

--(290 rows affected)

--POR MES
SELECT BusinessEntityID, JobTitle, HireDate, DATEPART(MONTH, HireDate) AS 'Mes Contrato'
FROM HumanResources.Employee
ORDER BY DATEPART(MONTH, HireDate);
GO

--POR D�A
SELECT BusinessEntityID, JobTitle, HireDate, DATEPART(DAY, HireDate) AS 'D�a Contrato'
FROM HumanResources.Employee
ORDER BY DATEPART(DAY, HireDate);
GO

--TODO COMBINADO
SELECT BusinessEntityID, JobTitle, HireDate, DATEPART(YEAR, HireDate) AS 'A�o Contrato',
	DATEPART(MONTH, HireDate) AS 'Mes Contrato', DATEPART(DAY, HireDate) AS 'D�a Contrato'
FROM HumanResources.Employee
ORDER BY DATEPART(YEAR, HireDate), DATEPART(MONTH, HireDate), DATEPART(day, HireDate);
GO


