-- Primeros ejemplos probando group by con funciones de Agregado

-- Problema:

--Trabajamos con la tabla "libros" de una librer�a.
--Eliminamos la tabla, si existe:

USE Tempdb
go
 if object_id('libros') is not null
  drop table libros;

-- Creamos la tabla:

 create table libros(
  codigo int identity,
  titulo varchar(40),
  autor varchar(30),
  editorial varchar(15),
  precio decimal(5,2),
  cantidad tinyint,
  primary key(codigo)
 )
 GO
-- Ingresamos algunos registros:

 insert into libros
  values('El aleph','Borges','Planeta',15,null);
 insert into libros
  values('Martin Fierro','Jose Hernandez','Emece',22.20,200);
 insert into libros
  values('Antologia poetica','J.L. Borges','Planeta',null,150);
 insert into libros
  values('Aprenda PHP','Mario Molina','Emece',18.20,null);
 insert into libros
  values('Cervantes y el quijote','Bioy Casares- J.L. Borges','Paidos',null,100);
 insert into libros
  values('Manual de PHP', 'J.C. Paez', 'Siglo XXI',31.80,120);
 insert into libros
  values('Harry Potter y la piedra filosofal','J.K. Rowling',default,45.00,90);
 insert into libros
  values('Harry Potter y la camara secreta','J.K. Rowling','Emece',null,100);
 insert into libros
  values('Alicia en el pais de las maravillas','Lewis Carroll','Paidos',22.50,200);
 insert into libros
  values('PHP de la A a la Z',null,null,null,0);
GO
SELECT * 
FROM Libros
GO

-- Queremos saber la cantidad de libros de cada editorial     --  utilizando la cl�usula "group by":

SELECT editorial, COUNT(*) AS 'Numero de Libros'
FROM Libros
GROUP BY editorial
GO

-- Hay 2 Editoriales con Null

--Editorial	N�mero de Libros
--NULL		2	
--Emece		3
--Paidos	2
--Planeta	2
--Siglo XXI	1

-- El resultado muestra los nombres de las editoriales y la cantidad de registros para cada valor del campo.
-- Los valores nulos se procesan como otro grupo.
select editorial, count(*) as [N�mero de Libros], precio
  from libros
  group by editorial
GO

--Msg 8120, Level 16, State 1, Line 70
--Column 'libros.precio' is invalid in the select list because it is not contained in either an aggregate function or the GROUP BY clause.
--Los campos que intervengan tienen que estar en una funcion de agregacion o en un group by, en este ejemplo precio es el que 
-- genera el error. Una posible soluci�n ser�a calcular la media de precios, o el precio m�ximo, etc.
-- Obtenemos la cantidad libros con precio no nulo de cada editorial:

--SOLUCI�N CON FUNCION DE AGREGACION 
SELECT editorial, count(*) as [N�mero de Libros], MAX(precio) AS 'Libro m�s caro'
  FROM libros
  GROUP BY editorial
GO

--SOLUCI�N CON GROUP BY
SELECT editorial, count(*) as [N�mero de Libros]
  FROM libros
  GROUP BY editorial,precio
GO

-- INTENTA QUE CUMPLA TODAS LAS CLAUSULAS, EN ESTE CASO
-- S�LO AGRUPAR�A LOS QUE PERTENECEN A LA MISMA EDITORIAL
-- Y ADEM�S TIENEN EL MISMO PRECIO.




 select editorial, count(precio)
  from libros
  group by editorial
GO
--editorial	(No column name)
--NULL	1
--Emece	2
--Paidos	1
--Planeta	1
--Siglo XXI	1
-- La salida muestra los nombres de las editoriales no nulas y la cantidad de registros de cada una, 
-- sin contar los que tienen precio nulo.
SELECT editorial, COUNT(precio)
  FROM libros
  WHERE editorial IS NOT NULL
  GROUP BY editorial
GO

-- EDITORIALES NO NULAS CON HAVING
SELECT editorial
FROM libros 
GROUP BY editorial
HAVING editorial IS NOT NULL
 GO

-- editorial
--Emece
--Paidos
--Planeta
--Siglo XXI



-- Total en dinero de los libros agrupados por editorial
-- CONTROLAMOS QUE NO SALGAN LAS EDITORIALES NULAS
SELECT editorial, SUM(precio) AS 'Suma de precios'
FROM libros
GROUP BY editorial
HAVING editorial IS NOT NULL 
GO

-- Obtenemos el m�ximo y m�nimo valor de los libros agrupados por editorial, en una sola sentencia:

SELECT editorial, 
	MAX(precio) AS 'Precio M�ximo', 
	MIN(precio) AS 'Precio M�nimo'
FROM libros
GROUP BY editorial
HAVING editorial IS NOT NULL 
-- WHERE editorial IS NOT NULL / SENTENCIAS SINONIMAS EN ESTE CASO 
GO

-- Calculamos el promedio del valor de los libros agrupados por editorial:

SELECT editorial, AVG(precio) AS 'Precio Medio'
FROM libros
GROUP BY editorial 
HAVING editorial IS NOT NULL
GO

--DAR FORMATO A LOS DATOS
SELECT editorial, CAST(AVG(precio) AS bigint) AS 'Precio Medio'
FROM libros
GROUP BY editorial 
HAVING editorial IS NOT NULL
GO
--editorial	Precio Medio
--Emece			20
--Paidos		22
--Planeta		15
--Siglo XXI		31


SELECT editorial, CONVERT(INT,AVG(precio))
FROM libros
GROUP BY editorial 
HAVING editorial IS NOT NULL
GO

SELECT editorial, CONVERT(float,AVG(precio))
FROM libros
GROUP BY editorial 
HAVING editorial IS NOT NULL
GO


-- ESTOS NO LOS HACEMOS, PODEMOS HACERLOS LUEGO....
-- PASAMOS A LA SIGUIENTE BASE DE DATOS.
--Es posible limitar la consulta con "where". Vamos a contar y agrupar por editorial considerando solamente los libros cuyo precio es menor a 30 pesos:

SELECT editorial, COUNT(*) 'Precio < 30'
FROM libros
WHERE precio<30
GROUP BY editorial
GO

-- Las editoriales que no tienen libros que cumplan la condici�n, no aparecen en la salida.
-- Para que aparezcan todos los valores de editorial, incluso los que devuelven cero o "null" en la columna de agregado, 
-- debemos emplear la palabra clave "all" al lado de "group by":

SELECT editorial, COUNT(*) 'Precio < 30'
FROM libros
WHERE precio<30
GROUP BY ALL editorial
GO

--------------------
-- sqlserverya 


--Primer problema: 
--Un comercio que tiene un stand en una feria registra en una tabla llamada "visitantes" algunos datos 
--de las personas que visitan o compran en su stand para luego enviarle publicidad de sus productos.
--1- Elimine la tabla "visitantes", si existe:
 if object_id('visitantes') is not null
  drop table visitantes
  go

--2- Cree la tabla con la siguiente estructura:
 create table visitantes(
  nombre varchar(30),
  edad tinyint,
  sexo char(1) default 'f',
  domicilio varchar(30),
  ciudad varchar(20) default 'Cordoba',
  telefono varchar(11),
  mail varchar(30) default 'no tiene',
  montocompra decimal (6,2)
 );


 insert into visitantes
  values ('Susana Molina',35,default,'Colon 123',default,null,null,59.80);
 insert into visitantes
  values ('Marcos Torres',29,'m',default,'Carlos Paz',default,'marcostorres@hotmail.com',150.50);
 insert into visitantes
  values ('Mariana Juarez',45,default,default,'Carlos Paz',null,default,23.90);
 insert into visitantes (nombre, edad,sexo,telefono, mail)
  values ('Fabian Perez',36,'m','4556677','fabianperez@xaxamail.com');
 insert into visitantes (nombre, ciudad, montocompra)
  values ('Alejandra Gonzalez','La Falda',280.50);
 insert into visitantes (nombre, edad,sexo, ciudad, mail,montocompra)
  values ('Gaston Perez',29,'m','Carlos Paz','gastonperez1@gmail.com',95.40);
 insert into visitantes
  values ('Liliana Torres',40,default,'Sarmiento 876',default,default,default,85);
 insert into visitantes
  values ('Gabriela Duarte',21,null,null,'Rio Tercero',default,'gabrielaltorres@hotmail.com',321.50);

  select * from visitantes
  GO


--4- Queremos saber la cantidad de visitantes de cada ciudad utilizando la cl�usula "group by" (4 filas devueltas)

SELECT ciudad, COUNT(*) AS 'Visitantes'
FROM visitantes
GROUP BY ciudad
GO

--ciudad		Visitantes
--Carlos Paz	3
--Cordoba		3
--La Falda		1
--Rio Tercero	1


--5- Queremos la cantidad visitantes con tel�fono no nulo, de cada ciudad (4 filas devueltas)

SELECT ciudad, COUNT(*) AS 'Telefono == NULL'
FROM visitantes
GROUP BY ciudad, telefono
HAVING telefono IS NULL
GO

--ciudad		Telefono == NULL
--Carlos Paz		3
--Cordoba			2
--La Falda			1
--Rio Tercero		1


--6- Necesitamos el total del monto de las compras agrupadas por sexo (3 filas)

SELECT sexo, SUM(montocompra) AS 'Total comprado'
FROM visitantes
GROUP BY sexo
GO

--sexo	Total comprado
--NULL		321.50
--f			449.20
--m			245.90


--7- Se necesita saber el m�ximo y m�nimo valor de compra agrupados por sexo y ciudad (6 filas)

SELECT ciudad, sexo, MAX(montocompra) AS 'Compra m�xima', MIN(montocompra) AS 'Compra m�nima'
FROM visitantes
GROUP BY ciudad, sexo
GO

--ciudad		sexo	Compra m�xima	Compra m�nima
--Rio Tercero	NULL		321.50			321.50
--Carlos Paz	f			23.90			23.90
--Cordoba		f			85.00			59.80
--La Falda		f			280.50			280.50
--Carlos Paz	m			150.50			95.40
--Cordoba		m			NULL			NULL


--8- Calcule el promedio del valor de compra agrupados por ciudad (4 filas)

SELECT ciudad, CONVERT(float,AVG(montocompra)) AS 'Compra media'
FROM visitantes
GROUP BY ciudad
GO

--ciudad		Compra media
--Carlos Paz		89,933333
--Cordoba			72,4
--La Falda			280,5
--Rio Tercero		321,5


--9- Cuente y agrupe por ciudad sin tener en cuenta los visitantes que no tienen mail (3 filas):

SELECT ciudad, COUNT(*) AS 'Visitantes con Email'
FROM visitantes
WHERE mail IS NOT NULL AND mail<>'no tiene'
GROUP BY ciudad
GO

--ciudad		Visitantes con Email
--Carlos Paz		2
--Cordoba			1
--Rio Tercero		1


--10- Realice la misma consulta anterior, pero use la palabra clave "all" para mostrar todos los 
--valores de ciudad, incluyendo las que devuelven cero o "null" en la columna de agregado (4 filas)

SELECT ciudad, COUNT(*) AS 'Visitantes con Email'
FROM visitantes
WHERE mail IS NOT NULL AND mail<>'no tiene'
GROUP BY ALL ciudad
GO

--ciudad		Visitantes con Email
--Carlos Paz		2
--Cordoba			1
--La Falda			0
--Rio Tercero		1	


----------------------------------------


--USANDO ADVENTUREWORKS2014
--DADO EL ESQUEMA/TABLA HumanResources.Employee

USE AdventureWorks2014
GO

SELECT * FROM HumanResources.Employee
GO 

sp_help [HumanResources.Employee]
GO

SELECT NationalIdNumber, HireDate
FROM HumanResources.Employee
ORDER BY HireDate DESC
GO

--OBTENER EL N� DE TRABAJADORES CONTRATADOS CADA A�O

SELECT DATEPART(year, HireDate) AS 'A�o de contrato', COUNT(*) AS 'Numero de contratos'
FROM HumanResources.Employee
GROUP BY DATEPART(year, HireDate)
GO

--A�o de contrato	Numero de contratos
--2006					1
--2007					6
--2008					74
--2009					148
--2010					38
--2011					16
--2012					4
--2013					3