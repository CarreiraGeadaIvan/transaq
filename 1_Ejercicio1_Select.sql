---- Order By con Funcion Fecha

CREATE DATABASE Visitas
GO
USE Visitas
GO

--DROP TABLE IF EXISTS visitas
--GO
-- if object_id('visitas') is not null
--  drop table visitas;

--2- Cr�ela con la siguiente estructura:
-- create table visitas (
--  numero int identity, 
--  nombre varchar(30) default 'Anonimo',
--  mail varchar(50),
--  pais varchar (20),
--  fecha datetime,
--  primary key(numero)
--);

--3- Ingrese algunos registros:
 --insert into visitas (nombre,mail,pais,fecha)
 -- values ('Ana Maria Lopez','AnaMaria@hotmail.com','Argentina','2006-10-10 10:10');
 --insert into visitas (nombre,mail,pais,fecha)
 -- values ('Gustavo Gonzalez','GustavoGGonzalez@hotmail.com','Chile','2006-10-10 21:30');
 --insert into visitas (nombre,mail,pais,fecha)
 -- values ('Juancito','JuanJosePerez@hotmail.com','Argentina','2006-10-11 15:45');
 --insert into visitas (nombre,mail,pais,fecha)
 -- values ('Fabiola Martinez','MartinezFabiola@hotmail.com','Mexico','2006-10-12 08:15');
 --insert into visitas (nombre,mail,pais,fecha)
 -- values ('Fabiola Martinez','MartinezFabiola@hotmail.com','Mexico','2006-09-12 20:45');
 --insert into visitas (nombre,mail,pais,fecha)
 -- values ('Juancito','JuanJosePerez@hotmail.com','Argentina','2006-09-12 16:20');
 --insert into visitas (nombre,mail,pais,fecha)
 -- values ('Juancito','JuanJosePerez@hotmail.com','Argentina','2006-09-15 16:25');

---- Ordene los registros por fecha, en orden descendente.

SELECT * FROM visitas ORDER BY fecha DESC 
GO

-- -- Muestre el nombre del usuario, pais y el nombre del mes, ordenado por pais (ascendente) y nombre 
---- del mes (descendente)

SELECT nombre, pais, datename(month, fecha) AS 'Mes' FROM visitas
ORDER BY pais ASC, datename(month, fecha) DESC
GO 

-- --  Muestre el pais, el mes, el d�a y la hora y ordene las visitas por 
-- -- nombre del mes, del d�a y la hora.

SELECT pais, mail, datename(month, fecha) AS 'Mes', datename(day, fecha) AS 'Dia', datename(hour, fecha) AS 'Hora'
FROM visitas 
ORDER BY 'Mes', 'Dia', 'Hora'
--ORDER BY 3,4,5
--ORDER BY datename(month, fecha), datename(day, fecha), datename(hour, fecha)
GO

---- Muestre los mail, pa�s, ordenado por pa�s, de todos los que visitaron la p�gina en octubre (4 
---- registros)

SELECT mail, pais FROM visitas
WHERE datename(month, fecha) = 'October'
ORDER BY pais
--ORDER BY 2
--ORDER BY datename(month, fecha)
GO

--DROP DATABASE Visitas
--GO