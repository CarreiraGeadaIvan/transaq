USE AdventureWorks2014
GO

DROP TABLE IF EXISTS Pruebas
GO

SELECT * INTO Pruebas
FROM Sales.SalesPersonQuotaHistory
GO

----BORRAR TODOS LOS REGISTROS
DELETE FROM Pruebas
GO

----- O

DELETE Pruebas
GO

------- BORRAR TODA LA TABLA

DROP TABLE Pruebas
GO

-- CONTAMOS EL NUMERO DE REGISTROS QUE HAY EN 
-- LA TABLA DESPU�S DEL BORRADO

SELECT COUNT(*) FROM Pruebas
GO

--(No column name)
--0

SELECT * FROM Pruebas
GO

----- BORRAR LOS REGISTROS CON UNA
----- SALESQUOTA > 200000

DELETE FROM Pruebas
WHERE SalesQuota > 200000
GO

SELECT * FROM Pruebas
GO

-- El valor de la columna StandardCost debe estar comprendido entre 12.00 y 14.00
-- y el valor de la columna SellEndDate debe ser NULL.
-- En el ejemplo se imprime tambi�n el valor desde la funci�n @@ROWCOUNT
-- para devolver el numero de filas eliminadas

DROP TABLE IF EXISTS Costes
GO

SELECT * INTO Costes 
FROM Production.ProductCostHistory
GO

DELETE Costes
	WHERE (StandardCost BETWEEN 12.00 AND 14.00)
	AND (EndDate IS NULL)
PRINT 'Filas borradas: '+CAST (@@ROWCOUNT AS CHAR(3))
GO

----------------------------
----					----
----	USANDO PUBS		----
----					----
----------------------------

USE pubs
GO

DROP TABLE IF EXISTS TituloAutores
GO

SELECT * INTO TituloAutores
FROM titleauthor
GO

-------------------------
---Borrar todos los libros que tengan la palabra 'computer' 
--- en su titulo


SELECT * FROM TituloAutores
GO

DELETE FROM TituloAutores
WHERE title_id IN (
	SELECT title_id
	FROM titles
	WHERE title LIKE '%computer%'
	)
GO

SELECT * INTO InventarioProducto 
FROM Production.ProductInventory
GO

SELECT * FROM InvetarioProducto
GO

--Creamos la tabla con la misma estructura que 
-- InvetarioProducto pero sin ning�n registro
-- para ello le metemos una condici�n where 
-- que no se va a cumplir nunca (1=0)

SELECT * INTO HistoricoBorrado
FROM Production.ProducInventory
WHERE 1=0
GO


DELETE InventarioProducto
	OUTPUT DELETED.*
	INTO HistoricoBorrado
	WHERE LocationID = 50
GO

SELECT * FROM HistoricoBorrado
GO

------------------------
--
--TRUNCATE
--
--

USE AdventureWorks2014
GO

SELECT * INTO Candidatos
FROM HumanResourcer.JobCandidate
GO

-----Contamos el n�mero de registros que hay en la tabla antes del borrado

SELECT COUNT(*) AS BeforeTruncateCount
FROM Candidatos
GO

-- Eliminamos el contenido de la tabla

TRUNCATE TABLE Candidatos
GO


--------------
--
--TRANSACCIONES
--
-- DELETE/TRUNCATE WITH ROLLBACK
USE tempdb
GO

CREATE TABLE Employee
	(
	Empid int IDENTITY NOT NULL,
	Name nchar(10) NULL,
	City nchar(10) NULL
	)

GO

INSERT INTO Employee VALUES ('Pepe', 'Lugo'),
				('Luis', 'Coru�a')
GO

BEGIN TRANSACTION
DELETE FROM Employee WHERE Empid=1
SELECT * FROM Employee
ROLLBACK TRANSACTION
SELECT * FROM Employee

BEGIN TRANSACTION 
TRUNCATE TABLE Employee
SELECT * FROM Employee
ROLLBACK TRANSACTION
SELECT * FROM Employee 
