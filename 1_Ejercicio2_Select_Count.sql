USE Tempdb
GO

DROP TABLE IF EXISTS libros
GO
--if object_id ('libros') is not null
--  drop table libros;
-- Creamos la tabla:

 create table libros(
  codigo int identity,
  titulo varchar(40) not null,
  autor varchar(20) default 'Desconocido',
  editorial varchar(20),
  precio decimal(6,2),
  primary key(codigo)
 );
--Ingresamos algunos registros:

 insert into libros
  values('El aleph','Borges','Emece',15.90);
 insert into libros
  values('Antolog�a po�tica','J. L. Borges','Planeta',null);
 insert into libros
  values('Alicia en el pais de las maravillas','Lewis Carroll',null,19.90);
 insert into libros
  values('Matematica estas ahi','Paenza','Siglo XXI',15);
 insert into libros
  values('Martin Fierro','Jose Hernandez',default,40);
 insert into libros
  values('Aprenda PHP','Mario Molina','Nuevo siglo',null);
 insert into libros
  values('Uno','Richard Bach','Planeta',20);

--
  select *
  from libros
  GO

  -- (7 row(s) affected)

-- Averig�emos la cantidad de libros usando la funci�n "count()":

SELECT COUNT(*) AS 'Cantidad de libros'
FROM libros 
GO

--Contamos los libros de la editorial "Planeta"

SELECT COUNT(*) AS 'Libros de una editorial'
FROM libros
WHERE editorial LIKE '%ane%'
GO

--Cuantas editoriales tengo publicando libros?
SELECT COUNT(DISTINCT editorial) AS 'Numero de editoriales'
FROM libros
GO

--De cuantos libros desconozco su editorial?
SELECT COUNT(*) AS 'Numero de libros sin editoriales'
FROM libros
WHERE editorial IS NULL
GO


------USANDO ADVENTURE WORKS
USE AdventureWorks2014
GO

SELECT JobTitle
FROM HumanResources.Employee
WHERE JobTitle IS NOT NULL
GO

SELECT COUNT(*) AS 'Empleados'
FROM HumanResources.Employee
GO

SELECT DISTINCT JobTitle AS 'Nombre del puesto'
FROM HumanResources.Employee
GO
SELECT COUNT(DISTINCT JobTitle) AS 'Cantidad de puestos'
FROM HumanResources.Employee
GO
--------
--CREAR TABLA EMPLEADOS SELECT/INTO

SELECT BusinessEntityID, NationalIDNumber, JobTitle
INTO Empleados
FROM HumanResources.Employee
WHERE Gender='F'
GO
SELECT COUNT(*) FROM Empleados
GO
SELECT COUNT(DISTINCT JobTitle) AS 'Cantidad de puestos'
FROM Empleados
GO

sp_help Empleados
GO

UPDATE Empleados SET JobTitle=NULL
WHERE BusinessEntityID=2
GO 
--Msg 515, Level 16, State 2, Line 103
--Cannot insert the value NULL into column 'JobTitle', table 'AdventureWorks2014.dbo.Empleados'; column does not allow nulls. UPDATE fails.
--The statement has been terminated.





