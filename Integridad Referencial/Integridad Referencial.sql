USE master
GO

DROP DATABASE IF EXISTS IntegridadReferencial
GO

CREATE DATABASE IntegridadReferencial
GO

USE IntegridadReferencial
GO

DROP TABLE IF EXISTS libros
GO
DROP TABLE IF EXISTS editoriales
GO

-- Table Parent
create table editoriales(
	codigo tinyint,
	nombre varchar(20),
	primary key (codigo));
GO
-- Table Child
create table libros(
	codigo int identity,
	titulo varchar(40),
	autor varchar(30),
	codigoeditorial tinyint default 3);
GO

insert into editoriales values(1,'Emece');
insert into editoriales values(2,'Planeta');
insert into editoriales values(3,'Siglo XXI');
GO
insert into libros values('El aleph','Borges',1);
insert into libros values('Martin Fierro','Jose Hernandez',2);
insert into libros values('Aprenda PHP','Mario Molina',2);
GO

------------------
-- Parent Table : Editoriales
-- Child Table : Libros

--Muestra la informaci�n sobre restricciones de una tabla
sp_helpconstraint libros;
GO

--A�adimos una restricci�n FK al campo 'codigoeditorial' de 'libros'
ALTER TABLE libros
ADD CONSTRAINT FK_libros_codigoeditorial
		FOREIGN KEY (codigoeditorial) 
		REFERENCES editoriales (codigo); 
Go
--Muestra la informaci�n sobre restricciones de una tabla
sp_helpconstraint libros;
GO
--Resultado del procedimiento almacenado
--DEFAULT on column codigoeditorial		DF__libros__codigoed__1ED998B2		(n/a)		(n/a)		(n/a)		(n/a)				((1))
--FOREIGN KEY							FK_libros_codigoeditorial			No Action	No Action	Enabled		Is_For_Replication	codigoeditorial
--	 	 	 	 																												REFERENCES PruebasDB.dbo.editoriales (codigo)

--Hint:
--See
--delete_action      update_action


--DESHABILITAR/HABILITAR UNA RESTRICCI�N
--Deshabilitar
ALTER TABLE libros
		NOCHECK CONSTRAINT FK_libros_codigoeditorial;
GO		
--DEFAULT on column codigoeditorial		DF__libros__codigoed__1ED998B2		(n/a)		(n/a)		(n/a)		(n/a)				((1))
--FOREIGN KEY							FK_libros_codigoeditorial			No Action	No Action	Disabled	Is_For_Replication	codigoeditorial
sp_helpconstraint libros;
GO	 	 	 	 																													REFERENCES PruebasDB.dbo.editoriales (codigo)
		
		
		
--Habilitar una restricci�n deshabilitada
ALTER TABLE libros
		CHECK CONSTRAINT FK_libros_codigoeditorial;
GO	
sp_helpconstraint libros;
GO

	
--Eliminar una restricci�n
ALTER TABLE libros
		DROP CONSTRAINT FK_libros_codigoeditorial;
		--DEFAULT on column codigoeditorial		DF__libros__codigoed__1ED998B2		(n/a)		(n/a)		(n/a)		(n/a)				((1))
GO
sp_helpconstraint libros
GO
------------------------------------------------
--INTEGRIDAD REFERENCIAL 
--
-- UN DELETE/UPDATE NO ACTION
--
------------------------------------------------

ALTER TABLE libros
ADD CONSTRAINT FK_libros_codigoeditorial
		FOREIGN KEY (codigoeditorial) REFERENCES editoriales(codigo)
GO
sp_helpconstraint libros;
GO
-- Al intentar borrar codigo en la Tabla Editoriales NO ACTION 
-- (No lo permite)
DELETE
	FROM editoriales
	WHERE codigo=2
GO
--Msg 547, Level 16, State 0, Line 110
--The DELETE statement conflicted with the REFERENCE constraint "FK_libros_codigoeditorial". The conflict occurred in database "IntegridadReferencial", table "dbo.libros", column 'codigoeditorial'.
--The statement has been terminated.


-- Debemos desactivar la foreign key para poder borrar algo
-- DISABLE CONSTRAINT

ALTER TABLE libros
		NOCHECK CONSTRAINT FK_libros_codigoeditorial
GO
sp_helpconstraint libros;
GO

-------------
--Tambien nos la podemos cargar directamente
-------------

ALTER TABLE libros
		DROP CONSTRAINT FK_libros_codigoeditorial;
GO

DELETE
	FROM editoriales
	WHERE codigo=2
GO
-- (1 row(s) affected)
	
SELECT * FROM Editoriales
GO

ALTER TABLE libros
		CHECK CONSTRAINT FK_libros_codigoeditorial
GO

sp_helpconstraint libros;
GO

--Caso 4: CASCADE

ALTER TABLE libros
ADD CONSTRAINT FK_libros_codigoeditorial
	FOREIGN KEY (codigoeditorial) REFERENCES editoriales (codigo)
	--ON DELETE CASCADE
	ON UPDATE CASCADE;
GO
sp_helpconstraint libros
GO
DELETE
	FROM editoriales
	WHERE codigo=2;
GO
-- (1 row(s) affected)

SELECT * FROM editoriales;
SELECT * FROM libros;	
GO
--Vemos c�mo se eliminan los registros de la tabla libros que apuntan al registro eliminado de la tabla editoriales
-- Al Borrar Registro en Editoriales borra los relacionados en Libros

-- Tabla Editoriales

--codigo	nombre
--1	Emece
--3	Siglo XXI

-- Tabla Libros
--codigo	titulo	autor	codigoeditorial
--1	El aleph	Borges	1

--Caso 2: SET NULL
ALTER TABLE libros
ADD CONSTRAINT FK_libros_codigoeditorial
	FOREIGN KEY (codigoeditorial) REFERENCES editoriales (codigo)
	ON DELETE     SET NULL
GO

DELETE
	FROM editoriales
	WHERE codigo=2;
GO

SELECT * FROM editoriales;
SELECT * FROM libros;	
GO

--Se ve como elimina la editorial 2 en la tabla padre, en la tabla hijo pone null en los libros con c�digo 2 y 3
--Ahora restablecemos los valores

-- Practicando INSERT y UPDATE en lugar de regenerar las Tablas
insert into editoriales values(2,'Planeta');

UPDATE libros
		SET codigoeditorial = 2
		WHERE codigoeditorial IS NULL;
GO


--Caso 3: SET DEFAULT
--Nota: el valor DEFAULT tiene que existir en la tabla editoriales (padre), si no, no apuntar�a a nada y se perder�a la integridad referencial


ALTER TABLE libros
ADD CONSTRAINT FK_libros_codigoeditorial
	FOREIGN KEY (codigoeditorial) REFERENCES editoriales (codigo)
	ON DELETE     SET DEFAULT;
GO
sp_help 'Editoriales'
GO
sp_help 'Libros'
GO
-- C�digo por defecto para Editorial 3
GO
DELETE
	FROM editoriales
	WHERE codigo=2;
GO

SELECT * FROM editoriales;
SELECT * FROM libros;	
GO
--Al eliminar un registro de la tabla padre, les asigna a sus hijos el valor del padre establecido por defecto

--codigo	titulo	autor	codigoeditorial
--1	El aleph	Borges	1
--2	Martin Fierro	Jose Hernandez	3
--3	Aprenda PHP	Mario Molina	3