---------------------------
------- VISTAS  -----------
---------------------------

USE pubs
GO

DROP VIEW IF EXISTS vTodos
GO

CREATE VIEW vTodos 
AS
	SELECT *
	FROM dbo.authors
GO

SELECT * FROM vTodos
GO

-----------
--PROCEDIMIENTOS ALMACENADOS DE LAS VISTAS
--------------------------------------------

sp_help vTodos
GO

sp_depends vTodos
GO

sp_helptext vTodos
GO


-------
--MODIFICAR VISTA
--------
ALTER VIEW vTodos (id_autor, id_apellidos, id_telefono)
AS
	SELECT au_id, au_lname, phone
	FROM dbo.authors
	WHERE state = 'CA'
GO

SELECT * FROM vTodos
GO

DROP VIEW vTodos
GO

---------
--CREAR VISTA DE MAS DE UNA TABLA
----------

USE AdventureWorks2014
GO

CREATE VIEW HumanResources.viewEmployeeHireDate
AS
	SELECT p.FirstName, p.LastName, e.HireDate
	FROM HumanResources.Employee AS e JOIN Person.Person AS p
	ON e.BusinessEntityID = p.BusinessEntityID
GO

SELECT FirstName, LastName, HireDate
FROM HumanResources.viewEmployeeHireDate
ORDER BY LastName
GO

DROP VIEW HumanResources.viewEmployeeHireDate
GO

--------------------------------
-- INSERCIONES A TRAVES DE LA VISTA
------------------------------------
-- CREAMOS LA TABLA
------------------------------------
USE pubs
GO

DROP TABLE IF EXISTS Nombres_Autores
GO
SELECT au_lname, au_fname
INTO Nombres_Autores
FROM Authors
GO

SELECT * FROM Nombres_Autores
ORDER BY au_lname
GO

--------------------------
--CREAMOS LA VISTA EN S�
--------------------------

DROP VIEW IF EXISTS vNombres_Autores
GO

CREATE VIEW vNombres_Autores (au_lname, au_fname)
AS 
	SELECT au_lname, au_fname
	FROM Nombres_Autores
GO

SELECT * FROM vNombres_Autores
ORDER BY au_lname
GO


---------------------------------------------
---------------------------------------------
--- INSERTAMOS CAMPOS A TRAV�S DE LA VISTA --
---------------------------------------------
---------------------------------------------

INSERT INTO vNombres_Autores
VALUES ('Arias', 'Luis')
GO

-- Comprobamos en la tabla origen

SELECT * FROM Nombres_Autores
ORDER BY au_lname
GO

-------------------------------------------------------------------------------
-- Las inserciones a traves de la vista deben 
-- cumplir con las restricciones de la tabla origen, si existiesen 
-- restricciones de no aceptar nulos, por ejemplo saltar�a un mensaje de error
-------------------------------------------------------------------------------

INSERT INTO vNombres_Autores
VALUES ('Garcia')
GO
--Msg 213, Level 16, State 1, Line 131
--Column name or number of supplied values does not match table definition.


DROP VIEW IF EXISTS vNombres_Autores1
GO 

CREATE VIEW vNombres_Autores1
AS
	SELECT au_lname, au_fname
	FROM Nombres_Autores
	WHERE au_fname LIKE 'A%'
GO

SELECT * FROM vNombres_Autores1
ORDER BY au_lname
GO


INSERT INTO vNombres_Autores1
VALUES ('ZAPATA', 'PEPE')
GO

--nos permite a�adir el valor aunque no cumple con la condicion de la vista
-- de que el nombre tiene que empezar por 'A'

SELECT * FROM Nombres_Autores
GO

SELECT * FROM vNombres_Autores1 ORDER BY au_lname
GO

DELETE vNombres_Autores1
WHERE au_lname = 'Zapata'
GO

UPDATE vNombres_Autores1
SET au_lname='Zapa'
WHERE au_lname='Zapata'
GO

-----------------
---- WITH CHECK OPTION
------------------

DROP VIEW IF EXISTS vNombres_Autores2
GO

CREATE VIEW vNombres_Autores2
AS
	SELECT au_lname, au_fname
	FROM Nombres_Autores
	WHERE au_fname LIKE 'A%'
	WITH CHECK OPTION
GO

INSERT INTO vNombres_Autores2
VALUES ('ZAPATA', 'PEPE')
GO
--Msg 550, Level 16, State 1, Line 190
--The attempted insert or update failed because the target view either specifies WITH CHECK OPTION 
--or spans a view that specifies WITH CHECK OPTION and one or more rows resulting from the 
--operation did not qualify under the CHECK OPTION constraint.
--The statement has been terminated.


INSERT INTO vNombres_Autores2
VALUES ('ZAPATA', 'Ana')
GO

--(1 row affected)

sp_helptext vNombres_Autores2
GO

ALTER VIEW vNombres_Autores2
WITH ENCRYPTION
AS 
	SELECT au_lname, au_fname
	FROM Nombres_Autores
	WHERE au_fname LIKE 'A%'
GO

sp_helptext vNombres_Autores2
GO


DROP TABLE IF EXISTS Algunos_Autores
DROP VIEW IF EXISTS vAlgunos_Autores
GO

SELECT au_lname, au_fname
INTO Algunos_Autores
FROM Authors
GO

CREATE VIEW vAlgunos_Autores (au_lanme, au_fname)
AS
	SELECT au_lname, au_fname
	FROM Algunos_Autores
GO

DROP TABLE Algunos_Autores
GO

SELECT * FROM vAlgunos_Autores
GO

INSERT INTO vAlgunos_Autores
VALUES ('Pepito', 'Perez')
GO

CREATE VIEW vAlgunos_Autores2 (au_lname, au_fname)
WITH SCHEMADINDING
AS 
	SELECT au_lname, au_fname
	FROM dbo.Algunos_Autores
GO

DROP TABLE Algunos_Autores
GO


INSERT INTO vAlgunos_Autores 
VALUES ('Lopez', 'Pedro')
GO
----ERROR

SELECT * FROM Algunos_Autores
GO
----ERROR


---------LO HACEMOS AHORA CON SCHEMABINDINGS
DROP TABLE IF EXISTS Algunos_Autores
GO

SELECT au_lname, au_fname
INTO Algunos_Autores
FROM Authors
GO

DROP VIEW IF EXISTS vAlgunos_Autores
GO

CREATE VIEW vAlgunos_Autores (au_lname, au_fname)
WITH SCHEMABINDING 
AS 
	SELECT au_lname, au_fname
	FROM dbo.Algunos_Autores --Hay que poner el nombre del schema (dbo)
GO

DROP TABLE Algunos_Autores
GO
---- NO DEJA PORQUE ESTA LINKADO A UNA VISTA


DROP VIEW IF EXISTS vTitulosMedia
GO

CREATE VIEW vTitulosMedia (categoria, precio_medio)
AS
	SELECT type, avg(price)
	FROM titles
	GROUP BY type
GO
